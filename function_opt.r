#phenotypic selection PS
#genomic selection + phenotypic selection GPS
#step 1 : NC crosses and production of N2 HD
#step  2: multiplication of N2 lines with the cost CM2
#step  3: multiplication of N3 lines with the cost CM3 et phenotyping with the cost CP3 or genotyping of N3 lines with the cost CG, selection of N4 lines and multiplication of N4 lines
#step 4: multiplication of N4 lines with the cost  CM4 et phenotyping with the cost CP4, sélection de NP parents pour le cycle suivant

#CT: total cost
#K: nb of cycles
#NP: nb ofparents
#NC: nb of crosses
#NDmax: maximum number of progenies with one cross
#NCmax: maximum number of crosses for one cycle
#Nref: nb of lines in the training population


#alpha: NP/(NC*N2)
#alpha2:N3/N2=0.2
#alpha3: (NC*N4)/(NC*N3)=N4/N3
#alpha4: NP/(NC*N4)
#alpha34=alpha3*alpha4
#alpha3=alpha^lambda
#alpha4=alpha^(1-lambda)

#cross_random2: without contribution threshold
#cross_random3: with contribution threshold
#cross_value_best_chr: without contribution threshold
#cross_value_best_chr2: with contribution threshold


progeny_nb_GPS <- function(CT,CC,CDH,CM2,CM3,CM4,CP4,CG,Nref,NP,NC,lambda,K,N2max,alpha2) {
  if (!requireNamespace("rootSolve", quietly = TRUE)) {
    stop("rootSolve needed for this function to work. Please install it.", call. = FALSE)
  }
  #library(rootSolve)
  fun <- function (N2) (Nref*CG+3*K*(NC*CC+N2*(CDH+CM2+(alpha2*CG))+(NP^lambda)*(alpha2*N2)^(1-lambda)*(CM3+CM4+CP4))-(CT))
  N2 <- round(uniroot(fun, c(0,N2max))$root)
  N3 <- round(0.2*N2)
  N4 <- round(((NP^lambda)*(alpha2*N2)^(1-lambda)))
  result <- c(NC,N2,N3,N4)

  names(result) <- c("NC","N2","N3","N4")
  return(result)
}


progeny_nb_GS <- function(CT,CC,CDH,CM2,CM3,CM4,CP4,CG,Nref,NP,NC,lambda,K,N2max,alpha2) {
  if (!requireNamespace("rootSolve", quietly = TRUE)) {
    stop("rootSolve needed for this function to work. Please install it.", call. = FALSE)
  }
  #library(rootSolve)
  fun <- function (N2) ((Nref*CG+2*K*(NC*CC+N2*(CDH+CM2+(alpha2*CG))+(NP^lambda)*(alpha2*N2)^(1-lambda)*(CM3+CM4+CP4))+2*K*(NC*CC+N2*CDH+(alpha2*N2)*CG))-(CT))
  N2 <- round(uniroot(fun, c(0,N2max))$root)
  N3 <- round(0.2*N2)
  N4 <- round(((NP^lambda)*(alpha2*N2)^(1-lambda)))
  result <- c(NC,N2,N3,N4)
  names(result) <- c("NC","N2","N3","N4")
  return(result)
}


cross_nb_GS <- function(CT,CC,CDH,CM2,CM3,CM4,CP4,CG,Nref,NP,n2=round(N2PS/NCPS),lambda,K,NCmax,N2max,alpha2) {
  if (!requireNamespace("rootSolve", quietly = TRUE)) {
    stop("rootSolve needed for this function to work. Please install it.", call. = FALSE)
  }
  #library(rootSolve)
  fun <- function (NC) ((Nref*CG+2*K*(NC*CC+n2*NC*(CDH+CM2+(alpha2*CG))+(NP^lambda)*(alpha2*n2*NC)^(1-lambda)*(CM3+CM4+CP4))+2*K*(NC*CC+alpha2*n2*NC*CDH+(alpha2*n2*NC)*CG))-(CT))
#  rm(NC)
  NC <- round(uniroot(fun, c(0,NCmax))$root)
  N2 <- progeny_nb_GS(CT=CT,CC=CC,CDH=CDH,CM2=CM2,CM3=CM3,CM4=CM4,CP4=CP4,CG=CG,Nref=Nref,NP=NP,NC=NC,lambda=lambda,K=K,N2max=N2max,alpha2=alpha2)["N2"]
  N4 <- round(((NP^lambda)*(alpha2*N2)^(1-lambda)))
  N3 <- round(0.2*N2)
  result <- c(NC,N2,N3,N4)
  names(result) <- c("NC","N2","N3","N4")
  return(result)
}


progeny_nb_GPS <- function(CT,CC,CDH,CM2,CM3,CM4,CP4,CG,Nref,NP,NC,lambda,K,N2max,alpha2) {
  if (!requireNamespace("rootSolve", quietly = TRUE)) {
    stop("rootSolve needed for this function to work. Please install it.", call. = FALSE)
  }
  #library(rootSolve)
  fun <- function (N2) (Nref*CG+3*K*(NC*CC+N2*(CDH+CM2+(alpha2*CG))+(NP^lambda)*(alpha2*N2)^(1-lambda)*(CM3+CM4+CP4))-(CT))
  N2 <- round(uniroot(fun, c(0,N2max))$root)
  N3 <- round(0.2*N2)
  N4 <- round(((NP^lambda)*(alpha2*N2)^(1-lambda)))
  result <- c(NC,N2,N3,N4)
  names(result) <- c("NC","N2","N3","N4")
  return(result)
}


cross_nb_GPS <- function(CT,CC,CDH,CM2,CM3,CM4,CP4,CG,Nref,NP,n2=round(N2PS/NCPS),lambda,K,NCmax,N2max,alpha2) {
  if (!requireNamespace("rootSolve", quietly = TRUE)) {
    stop("rootSolve needed for this function to work. Please install it.", call. = FALSE)
  }
  #library(rootSolve)
  fun <- function (NC) (Nref*CG+3*K*(NC*CC+n2*NC*(CDH+CM2+(alpha2*CG))+(NP^lambda)*(alpha2*n2*NC)^(1-lambda)*(CM3+CM4+CP4))-(CT))
#  rm(NC)
  NC <- round(uniroot(fun, c(0,NCmax))$root)
  N2 <- progeny_nb_GPS(CT=CT,CC=CC,CDH=CDH,CM2=CM2,CM3=CM3,CM4=CM4,CP4=CP4,CG=CG,Nref=Nref,NP=NP,NC=NC,lambda=lambda,K=K,N2max=N2max,alpha2=alpha2)["N2"]
  N4 <- round(((NP^lambda)*(alpha2*N2)^(1-lambda)))
  N3 <- round(0.2*N2)
  result <- c(NC,N2,N3,N4)
  names(result) <- c("NC","N2","N3","N4")
  return(result)
}



progeny_nb_PS <- function(CT,CC,CDH,CM2,CM3,CM4,CP3,CP4,NP,NC,lambda,N2max,alpha2,K) {

  if (!requireNamespace("rootSolve", quietly = TRUE)) {
    stop("rootSolve needed for this function to work. Please install it.", call. = FALSE)
  }
   #library(rootSolve)
  fun <- function (N2) (3*K*(NC*CC+N2*(CDH+CM2+(alpha2*(CM3+CP3)))+((NP^lambda)*(alpha2*N2)^(1-lambda))*(CM4+CP4))-CT)
  N2 <- round(uniroot(fun, c(0,N2max))$root)
  N3 <- round(0.2*N2)
  N4 <- round(((NP^lambda)*(alpha2*N2)^(1-lambda)))
  result <- c(NC,N2,N3,N4)
  names(result) <- c("NC","N2","N3","N4")
  return(result)
}


cycle_GS_save=function(header2,year=2,cross,pro.sp,parameters,scenario,run,estimated_effects,names=1001:1100,n3s)
{

parameter=parameters[scenario,]

N4=parameters[scenario,"GS.N4"]
NC=parameters[scenario,"GS.NC"]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]
crossing=parameters[scenario,"crossing"]

progeny.sp=pro.sp

noqtl=which(effects==0)
progeny2.sp=progeny.sp
for (i in 1:NC)
{
progeny2.sp[[i]]=progeny.sp[[i]][,noqtl]
}


gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV(progeny2.sp[[i]],estimated_effects)

geno.tmp=progeny.sp[[i]]
geno2.tmp=progeny2.sp[[i]]

ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]
geno2.tmp=geno2.tmp[ord,]

gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp
progeny2.sp[[i]]=geno2.tmp



}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,gebv3.sp)
		}

progeny4=NULL


fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {


progeny3=NULL
gebv3=NULL
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 1:NC)
{
progeny3=rbind(progeny3,progeny.sp[[i]])
gebv3=c(gebv3,gebv3.sp[[i]])

}


sel=rev(order(gebv3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}



progeny42=progeny4[,noqtl]



gebv4=generate_BV(progeny42,estimated_effects)



ord=rev(order(gebv4))

gebv4=gebv4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]



if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),paste(fam4[,1],fam4[,2],sep="_"))
gebv4.sp=split(gebv4,paste(fam4[,1],fam4[,2],sep="_"))
NC=length(progeny4.sp)

		if (sizep=="balanced")
		{
		av=unlist(lapply(gebv4.sp,mean))
tmp=gebv4.sp[rev(order(av))][1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)

fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))
		}


		if (sizep=="proportional")
		{
		nps=proportional(NP,NC,gebv4.sp)
		
progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))

for (i in 1:NC)
{
if(nps[i]>0)
{

progeny=rbind(progeny,progeny4.sp[[i]][1:nps[i],])
}
}
}






} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}


rownames(progeny)=names
fam=cbind(names,fam)

nam=paste(header2,"_GS_h2_",h2,"_scenario",scenario,"_bycross4=",bycross4,"_bycrossp=",bycrossp,"_size4=",size4,"_sizep=",sizep,"_run",run,"_year",year,"_crossing=",crossing,sep="")
write.table(progeny,paste(nam,"_progeny.txt",sep=""),quote=F,sep="\t")
write.table(fam,paste(nam,"_pedigree.txt",sep=""),quote=F,sep="\t",row.names=F,col.names=F)



}


cycle_GPS_NC=function(header2,header="GPS",year=4,cross,pro.sp,parameters,scenario,run,effects,estimated_effects,names=1001:1100,n3s,nb4="GPSn2.N4",nc="GPSn2.NC")
{

parameter=parameters[scenario,]

N4=parameters[scenario,nb4]
NC=parameters[scenario,nc]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]
crossing=parameters[scenario,"crossing"]

progeny.sp=pro.sp
progeny2.sp=vector("list",NC)

noqtl=which(effects==0)
for (i in 1:NC)
{
progeny2.sp[[i]]=progeny.sp[[i]][,noqtl]
}



gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV(progeny2.sp[[i]],estimated_effects)

geno.tmp=progeny.sp[[i]]


ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]


gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp
}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,gebv3.sp)
		}




#selection of the n4s with the best gebv
progeny4=NULL

#toto
fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {

nam=paste(header2,"_",header,"_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")

i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


gebv3=gebv3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

gebv3=c(gebv3,gebv3.sp[[i]])

}

progeny3=fread(nam,data.table=F)

sel=rev(order(gebv3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}




# N4 phenotyping

pheno4=prod_noise(generate_BV(progeny4,effects),h2)



ord=rev(order(pheno4))

pheno4=pheno4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]


#NP selection

if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),paste(fam4[,1],fam4[,2],sep="/"))
pheno4.sp=split(pheno4,paste(fam4[,1],fam4[,2],sep="/"))
NC=length(progeny4.sp)

		if (sizep=="balanced")
		{
av=unlist(lapply(pheno4.sp,mean))


tmp=progeny4.sp[rev(order(av))][1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)
fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))

 
		}


		if (sizep=="proportional")
		{
		nps=proportional(NP,NC,pheno4.sp)
		

progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))

for (i in 1:NC)
{
if(nps[i]>0)
{
tmp=progeny4.sp[[i]]
progeny=rbind(progeny,tmp[1:nps[i],])
}
}
}






} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}


rownames(progeny)=names
fam=cbind(names,fam)


result=vector("list",5)
result[[1]]=fam
result[[2]]=progeny
result[[3]]=pheno4
result[[4]]=progeny4
result[[5]]=fam4

return(result)


}


cycle_GPS=function(header2,header,year,cross,pro.sp,parameters,scenario,run,effects,estimated_effects,names,n3s)
{

N4=as.numeric(parameters["N4"])
NC=as.numeric(parameters["NC"])
NP=as.numeric(parameters["NP"])
h2=as.numeric(parameters["h2"])
bycross4=as.numeric(parameters["bycross4"])
bycrossp=as.numeric(parameters["bycrossp"])
size4=as.numeric(parameters["size4"])
sizep=as.numeric(parameters["sizep"])
crossing=parameters["crossing"]


progeny.sp=pro.sp
progeny2.sp=vector("list",NC)

noqtl=which(effects==0)
for (i in 1:NC)
{
progeny2.sp[[i]]=progeny.sp[[i]][,noqtl]
}


gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV(progeny2.sp[[i]],estimated_effects)

geno.tmp=progeny.sp[[i]]

ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]

gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp
}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,gebv3.sp)
		}

	
progeny4=NULL

fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {


nam=paste(header2,"_",header,"_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")

i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


gebv3=gebv3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

gebv3=c(gebv3,gebv3.sp[[i]])

}

progeny3=fread(nam,data.table=F)
tbv3=generate_BV(progeny3,effects)
accu=ac(tbv3,gebv3)
rmse=mse(tbv3,gebv3)

sel=rev(order(gebv3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}


pheno4=prod_noise(generate_BV(progeny4,effects),h2)



ord=rev(order(pheno4))

pheno4=pheno4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]


if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),paste(fam4[,1],fam4[,2],sep="/"))
pheno4.sp=split(pheno4,paste(fam4[,1],fam4[,2],sep="/"))
NC=length(progeny4.sp)

		if (sizep=="balanced")
		{
av=unlist(lapply(pheno4.sp,mean))


tmp=progeny4.sp[rev(order(av))][1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)
fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))

 
		}


		if (sizep=="proportional")
		{
		nps=proportional(NP,NC,pheno4.sp)
		
progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))

for (i in 1:NC)
{
if(nps[i]>0)
{
tmp=progeny4.sp[[i]]
progeny=rbind(progeny,tmp[1:nps[i],])
}
}
}






} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}

names(tbv3)=names(gebv3)=rownames(progeny3)

ord=rev(order(tbv3))
tbv3=tbv3[ord]
ord=rev(order(gebv3))

gebv3=gebv3[ord]



TBV=as.vector(generate_BV(progeny,effects))
names(TBV)=rownames(progeny)
progeny=progeny[rev(order(TBV)),]
TBV=TBV[rev(order(TBV))]
TBV200=mean(TBV)
TBV10=mean(TBV[1:10])
TBV25=mean(TBV[1:25])
TBV50=mean(TBV[1:50])
TBV100=mean(TBV[1:100])


rate10=sum(names(tbv3)[1:10]%in%names(gebv3)[1:N4])/10
rate25=sum(names(tbv3)[1:25]%in%names(gebv3)[1:N4])/25
rate50=sum(names(tbv3)[1:50]%in%names(gebv3)[1:N4])/50
rate100=sum(names(tbv3)[1:100]%in%names(gebv3)[1:N4])/100
rate200=sum(names(tbv3)[1:200]%in%names(gebv3)[1:N4])/200
rate=sum(names(tbv3)[1:N4]%in%names(gebv3)[1:N4])/N4


rateNP10=sum(names(tbv3)[1:10]%in%names(TBV))/10
rateNP25=sum(names(tbv3)[1:25]%in%names(TBV))/25
rateNP50=sum(names(tbv3)[1:50]%in%names(TBV))/50
rateNP100=sum(names(tbv3)[1:100]%in%names(TBV))/100
rateNP200=sum(names(tbv3)[1:200]%in%names(TBV))/200


progeny.qtl=progeny[,effects!=0]
nbqtl200=mean(apply(progeny.qtl,1,sum))
nbqtl100=mean(apply(progeny.qtl[1:100,],1,sum))
nbqtl50=mean(apply(progeny.qtl[1:50,],1,sum))
nbqtl10=mean(apply(progeny.qtl[1:10,],1,sum))
nbqtl25=mean(apply(progeny.qtl[1:25,],1,sum))

nb.al=length(unlist(apply(progeny,2,unique)))/(2*ncol(progeny))
var.tbv=var(TBV)



strategy=paste(header2,header,sep="_")

res=c(strategy,scenario,run,year,parameters["CG"],parameters["K"],bycross4,bycrossp,size4,sizep,h2,NC,parameters["N2"],N3,N4,TBV10,TBV25,TBV50,TBV100,TBV200,rate10,rate25,rate50,rate100,rate200,rate,rateNP10,rateNP25,rateNP50,rateNP100,rateNP200,nbqtl10,nbqtl25,nbqtl50,nbqtl100,nbqtl200,nb.al,accu,rmse,var.tbv)
write.table(t(data.frame(res)),paste(header2,"_",header,"_scenario",scenario,"_run",run,"_stat.txt",sep=""),append=T,col.names=F,row.names=F,quote=F,sep="\t")

rownames(progeny)=names
fam=cbind(names,fam)


result=vector("list",5)
result[[1]]=fam
result[[2]]=progeny
result[[3]]=pheno4
result[[4]]=progeny4
result[[5]]=fam4

return(result)


}




cycle_GS=function(header2,header,year,cross,pro.sp,parameters,scenario,run,effects,estimated_effects,names,n3s)
{

N4=as.numeric(parameters["N4"])
NC=as.numeric(parameters["NC"])
NP=as.numeric(parameters["NP"])
h2=as.numeric(parameters["h2"])
bycross4=as.numeric(parameters["bycross4"])
bycrossp=as.numeric(parameters["bycrossp"])
size4=as.numeric(parameters["size4"])
sizep=as.numeric(parameters["sizep"])
crossing=parameters["crossing"]


progeny.sp=pro.sp
progeny2.sp=vector("list",NC)

noqtl=which(effects==0)
for (i in 1:NC)
{
progeny2.sp[[i]]=progeny.sp[[i]][,noqtl]
}



gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV(progeny2.sp[[i]],estimated_effects)

geno.tmp=progeny.sp[[i]]

ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]


gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp
}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(NP,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(NP,NC,gebv3.sp)
		}

	
progeny4=NULL

fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {


nam=paste(header2,"_",header,"_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")

i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


gebv3=gebv3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

gebv3=c(gebv3,gebv3.sp[[i]])

}

progeny3=fread(nam,data.table=F)
tbv3=generate_BV(progeny3,effects)
accu=ac(tbv3,gebv3)
rmse=mse(tbv3,gebv3)
sel=rev(order(gebv3))[1:NP]
progeny=progeny3[sel,]
fam=fam3[sel,]


}


names(tbv3)=names(gebv3)=rownames(progeny3)

ord=rev(order(tbv3))
tbv3=tbv3[ord]
ord=rev(order(gebv3))
gebv3=gebv3[ord]



TBV=as.vector(generate_BV(progeny,effects))
names(TBV)=rownames(progeny)
progeny=progeny[rev(order(TBV)),]
TBV=TBV[rev(order(TBV))]
TBV200=mean(TBV)
TBV10=mean(TBV[1:10])
TBV25=mean(TBV[1:25])
TBV50=mean(TBV[1:50])
TBV100=mean(TBV[1:100])



rate10=sum(names(tbv3)[1:10]%in%names(gebv3)[1:N4])/10
rate25=sum(names(tbv3)[1:25]%in%names(gebv3)[1:N4])/25
rate50=sum(names(tbv3)[1:50]%in%names(gebv3)[1:N4])/50
rate100=sum(names(tbv3)[1:100]%in%names(gebv3)[1:N4])/100
rate200=sum(names(tbv3)[1:200]%in%names(gebv3)[1:N4])/200
rate=sum(names(tbv3)[1:N4]%in%names(gebv3)[1:N4])/N4


rateNP10=sum(names(tbv3)[1:10]%in%names(TBV))/10
rateNP25=sum(names(tbv3)[1:25]%in%names(TBV))/25
rateNP50=sum(names(tbv3)[1:50]%in%names(TBV))/50
rateNP100=sum(names(tbv3)[1:100]%in%names(TBV))/100
rateNP200=sum(names(tbv3)[1:200]%in%names(TBV))/200

progeny.qtl=progeny[,effects!=0]
nbqtl200=mean(apply(progeny.qtl,1,sum))
nbqtl100=mean(apply(progeny.qtl[1:100,],1,sum))
nbqtl50=mean(apply(progeny.qtl[1:50,],1,sum))
nbqtl10=mean(apply(progeny.qtl[1:10,],1,sum))
nbqtl25=mean(apply(progeny.qtl[1:25,],1,sum))


nb.al=length(unlist(apply(progeny,2,unique)))/(2*ncol(progeny))
var.tbv=var(TBV)



strategy=paste(header2,header,sep="_")

res=c(strategy,scenario,run,year,parameters["CG"],parameters["K"],bycross4,bycrossp,size4,sizep,h2,NC,parameters["N2"],N3,N4,TBV10,TBV25,TBV50,TBV100,TBV200,rate10,rate25,rate50,rate100,rate200,rate,rateNP10,rateNP25,rateNP50,rateNP100,rateNP200,nbqtl10,nbqtl25,nbqtl50,nbqtl100,nbqtl200,nb.al,accu,rmse,var.tbv)
write.table(t(data.frame(res)),paste(header2,"_",header,"_scenario",scenario,"_run",run,"_stat.txt",sep=""),append=T,col.names=F,row.names=F,quote=F,sep="\t")


rownames(progeny)=names
fam=cbind(names,fam)



result=vector("list",2)
result[[1]]=fam
result[[2]]=progeny

return(result)


}


cycle_GS_NC=function(header2,header="GS",year=4,cross,pro.sp,parameters,scenario,run,effects,estimated_effects,names=1001:1100,n3s)
{

parameter=parameters[scenario,]

NC=parameters[scenario,"GSn2.NC"]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]
crossing=parameters[scenario,"crossing"]


progeny.sp=pro.sp
progeny2.sp=vector("list",NC)

noqtl=which(effects==0)
for (i in 1:NC)
{
progeny2.sp[[i]]=progeny.sp[[i]][,noqtl]
}


gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV(progeny2.sp[[i]],estimated_effects)

geno.tmp=progeny.sp[[i]]

ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]


gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp

}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(NP,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(NP,NC,gebv3.sp)
		}

	

progeny4=NULL

fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {


nam=paste(header2,"_",header,"_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")

i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


gebv3=gebv3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{

fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

gebv3=c(gebv3,gebv3.sp[[i]])

}

progeny3=fread(nam,data.table=F)

sel=rev(order(gebv3))[1:NP]
progeny=progeny3[sel,]
fam=fam3[sel,]


}



result=vector("list",2)
result[[1]]=fam
result[[2]]=progeny


return(result)


}




cycle_GPS_gblup=function(header2,header="GPS",year=4,cross,pro.sp,parameters,scenario,run,effects,names=1001:1100,n3s,geno.train,pheno.train)
{

parameter=parameters[scenario,]

N4=parameters[scenario,"GPS.N4"]
NC=parameters[scenario,"GPS.NC"]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]
crossing=parameters[scenario,"crossing"]


progeny.sp=pro.sp
progeny2.sp=vector("list",NC)

noqtl=which(effects==0)
for (i in 1:NC)
{
progeny2.sp[[i]]=progeny.sp[[i]][,noqtl]
}



gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV_gblup(geno.train,pheno.train,progeny2.sp[[i]])

geno.tmp=progeny.sp[[i]]

ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]

gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp
}


	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,gebv3.sp)
		}

	
progeny4=NULL

fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}


 } else {


nam=paste(header2,"_",header,"_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")

i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


gebv3=gebv3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

gebv3=c(gebv3,gebv3.sp[[i]])

}

progeny3=fread(nam,data.table=F)

sel=rev(order(gebv3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}



pheno4=prod_noise(generate_BV(progeny4,effects),h2)



ord=rev(order(pheno4))

pheno4=pheno4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]


if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),paste(fam4[,1],fam4[,2],sep="/"))
pheno4.sp=split(pheno4,paste(fam4[,1],fam4[,2],sep="/"))
NC=length(progeny4.sp)

		if (sizep=="balanced")
		{
av=unlist(lapply(pheno4.sp,mean))



tmp=progeny4.sp[rev(order(av))][1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)

fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))

 
		}


		if (sizep=="proportional")
		{
		nps=proportional(NP,NC,pheno4.sp)
		


progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))

for (i in 1:NC)
{
if(nps[i]>0)
{
tmp=progeny4.sp[[i]]
progeny=rbind(progeny,tmp[1:nps[i],])
}
}
}






} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}


rownames(progeny)=names
fam=cbind(names,fam)


result=vector("list",5)
result[[1]]=fam
result[[2]]=progeny
result[[3]]=pheno4
result[[4]]=progeny4
result[[5]]=fam4

return(result)


}



cycle_GPS_withqtl=function(header2,header="GPS",year=4,cross,pro.sp,parameters,scenario,run,effects,estimated_effects,names=1001:1100,n3s)
{

parameter=parameters[scenario,]

N4=parameters[scenario,"GPS.N4"]
NC=parameters[scenario,"GPS.NC"]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]
crossing=parameters[scenario,"crossing"]


progeny.sp=progeny2.sp=pro.sp



gebv3.sp=vector("list",NC)
for (i in 1:NC)
{

gebv.tmp=generate_BV(progeny2.sp[[i]],estimated_effects)

geno.tmp=progeny.sp[[i]]

ord=rev(order(gebv.tmp))

gebv.tmp=gebv.tmp[ord]
geno.tmp=geno.tmp[ord,]

gebv3.sp[[i]]=gebv.tmp
progeny.sp[[i]]=geno.tmp
}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,gebv3.sp)
		}




progeny4=NULL

fam4=cross[rep(1:nrow(cross),n4s),1:2]

for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {


progeny3=NULL
gebv3=NULL
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 1:NC)
{
progeny3=rbind(progeny3,progeny.sp[[i]])
gebv3=c(gebv3,gebv3.sp[[i]])

}


sel=rev(order(gebv3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}


pheno4=prod_noise(generate_BV(progeny4,effects),h2)

neweffects=estimate_effect(pheno4,progeny4)

ord=rev(order(pheno4))

pheno4=pheno4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]



if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),paste(fam4[,1],fam4[,2],sep="/"))
pheno4.sp=split(pheno4,paste(fam4[,1],fam4[,2],sep="/"))
NC=length(progeny4.sp)

		if (sizep=="balanced")
		{
av=unlist(lapply(pheno4.sp,mean))
#tmp=progeny4.sp[rev(order(av))]


tmp=progeny4.sp[rev(order(av))][1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)
#test=progeny4.sp[rev(order(av))][1:NP]
#test2=pheno4.sp[rev(order(av))][1:NP]
fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))

 
		}


		if (sizep=="proportional")
		{
		nps=proportional(NP,NC,pheno4.sp)
		

progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))

for (i in 1:NC)
{
if(nps[i]>0)
{
tmp=progeny4.sp[[i]]
progeny=rbind(progeny,tmp[1:nps[i],])
}
}
}




} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}


rownames(progeny)=names
fam=cbind(names,fam)

nam=paste(header2,"_",header,"_h2_",h2,"_scenario",scenario,"_bycross4=",bycross4,"_bycrossp=",bycrossp,"_size4=",size4,"_sizep=",sizep,"_run",run,"_year",year,"_crossing=",crossing,sep="")
write.table(progeny,paste(nam,"_progeny.txt",sep=""),quote=F,sep="\t")
write.table(fam,paste(nam,"_pedigree.txt",sep=""),quote=F,sep="\t",row.names=F,col.names=F)
write.table(data.frame(neweffects),paste(nam,"_neweffects.txt",sep=""),quote=F,sep="\t",row.names=F,col.names=F)
write.table(progeny4,paste(nam,"_progeny4.txt",sep=""),quote=F,sep="\t")

}





cycle_PS=function(header2,year=4,cross,pro.sp,parameters,scenario,run,effects,names=1001:1100,n3s,h2)
{

parameter=parameters[scenario,]

N4=parameters[scenario,"PS.N4"]
NC=parameters[scenario,"PS.NC"]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]


progeny.sp=pro.sp


pheno3.sp=vector("list",NC)
for (i in 1:NC)
{

pheno.tmp=prod_noise(generate_BV(progeny.sp[[i]],effects),h2)

geno.tmp=progeny.sp[[i]]

ord=rev(order(pheno.tmp))

pheno.tmp=pheno.tmp[ord]
geno.tmp=geno.tmp[ord,]

pheno3.sp[[i]]=pheno.tmp
progeny.sp[[i]]=geno.tmp
}

	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,pheno3.sp)
		}

	

progeny4=NULL

fam4=cross[rep(1:nrow(cross),n4s),1:2]



for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {

######
nam=paste(header2,"_PS_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")

i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


pheno3=pheno3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{
#progeny3=rbind(progeny3,progeny.sp[[i]])
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

pheno3=c(pheno3,pheno3.sp[[i]])

}

progeny3=fread(nam,data.table=F)

sel=rev(order(pheno3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}



pheno4=prod_noise(generate_BV(progeny4,effects),h2)



ord=rev(order(pheno4))

pheno4=pheno4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]


if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),as.factor(paste(fam4[,1],fam4[,2],sep="/")))
pheno4.sp=split(pheno4,as.factor(paste(fam4[,1],fam4[,2],sep="/")))
NC=length(progeny4.sp)
av=unlist(lapply(pheno4.sp,mean))
progeny4.sp=progeny4.sp[rev(order(av))]
pheno4.sp=pheno4.sp[rev(order(av))]

		if (sizep=="balanced")
		{
tmp=progeny4.sp[1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)
fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))
		}


		if (sizep=="proportional")
		{
nps=proportional(NP,NC,pheno4.sp)

progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))
nps=nps[nps>0]

for (i in 1:length(nps))
{

tmp=progeny4.sp[[i]]
progeny=rbind(progeny,tmp[1:nps[i],])

}
}



} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}


rownames(progeny)=names
fam=cbind(names,fam)

result=vector("list",2)
result[[1]]=fam
result[[2]]=progeny
return(result)
}




cycle_PS=function(header2,header=header,year=year,cross=cross.rand.rand2,pro.sp=res,parameters=param,scenario=scenario,run=run,effects=effects,names=names,n3s=n3s)
{

N4=as.numeric(parameters["N4"])
NC=as.numeric(parameters["NC"])
NP=as.numeric(parameters["NP"])
h2=as.numeric(parameters["h2"])
bycross4=as.numeric(parameters["bycross4"])
bycrossp=as.numeric(parameters["bycrossp"])
size4=as.numeric(parameters["size4"])
sizep=as.numeric(parameters["sizep"])
crossing=parameters["crossing"]

progeny.sp=pro.sp

pheno3.sp=vector("list",NC)
for (i in 1:NC)
{

pheno.tmp=prod_noise(generate_BV(progeny.sp[[i]],effects),h2)

geno.tmp=progeny.sp[[i]]

ord=rev(order(pheno.tmp))

pheno.tmp=pheno.tmp[ord]
geno.tmp=geno.tmp[ord,]

pheno3.sp[[i]]=pheno.tmp
progeny.sp[[i]]=geno.tmp
}



	if (bycross4==1)
	{
		if (size4=="balanced")
		{
		n4s=balance(N4,NC)
		}


		if (size4=="proportional")
		{
		n4s=proportional(N4,NC,pheno3.sp)
		}

	

progeny4=NULL


fam4=cross[rep(1:nrow(cross),n4s),1:2]


for (i in 1:NC)
{
if(n4s[i]>0)
{

progeny4=rbind(progeny4,progeny.sp[[i]][1:n4s[i],])


}
}




 } else {

######
nam=paste(header2,"_",header,"_scenario",scenario,"_run",run,"_year",year,"_progeny.tmp.txt",sep="")


i=1
fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F)


pheno3=pheno3.sp[[i]]
fam3=cross[rep(1:nrow(cross),n3s),1:2]
for (i in 2:NC)
{

fwrite(data.frame(progeny.sp[[i]]),nam,quote=F,row.names=F,col.names=F,append=T)

pheno3=c(pheno3,pheno3.sp[[i]])

}

progeny3=fread(nam,data.table=F)

tbv3=generate_BV(progeny3,effects)
accu=ac(tbv3,pheno3)
rmse=mse(tbv3,pheno3)



sel=rev(order(pheno3))[1:N4]
progeny4=progeny3[sel,]
fam4=fam3[sel,]


}



pheno4=prod_noise(generate_BV(progeny4,effects),h2)



ord=rev(order(pheno4))

pheno4=pheno4[ord]
progeny4=progeny4[ord,]
fam4=fam4[ord,]


if (bycrossp==1)
{

progeny4.sp=split(data.frame(progeny4),as.factor(paste(fam4[,1],fam4[,2],sep="/")))
pheno4.sp=split(pheno4,as.factor(paste(fam4[,1],fam4[,2],sep="/")))
NC=length(progeny4.sp)
av=unlist(lapply(pheno4.sp,mean))
progeny4.sp=progeny4.sp[rev(order(av))]
pheno4.sp=pheno4.sp[rev(order(av))]

		if (sizep=="balanced")
		{
tmp=progeny4.sp[1:NP]
progeny=matrix(unlist(lapply(tmp,function(i) as.vector(i[1,]))),ncol=ncol(progeny4),byrow=T)
fam=data.frame(matrix(unlist(strsplit(names(tmp),"/")),ncol=2,byrow=T))
		}


		if (sizep=="proportional")
		{
nps=proportional(NP,NC,pheno4.sp)

progeny=NULL


fam=data.frame(matrix(unlist(strsplit(rep(names(progeny4.sp),nps),"/")),ncol=2,byrow=T))
nps=nps[nps>0]

for (i in 1:length(nps))
{

tmp=progeny4.sp[[i]]
progeny=rbind(progeny,tmp[1:nps[i],])

}
}



} else {

progeny=progeny4[1:NP,]
fam=fam4[1:NP,]

}


#############
###############



names(tbv3)=names(pheno3)=rownames(progeny3)

ord=rev(order(tbv3))
tbv3=tbv3[ord]
ord=rev(order(pheno3))

pheno3=pheno3[ord]



TBV=as.vector(generate_BV(progeny,effects))
names(TBV)=rownames(progeny)
progeny=progeny[rev(order(TBV)),]
TBV=TBV[rev(order(TBV))]
TBV200=mean(TBV)
TBV10=mean(TBV[1:10])
TBV25=mean(TBV[1:25])
TBV50=mean(TBV[1:50])
TBV100=mean(TBV[1:100])

rate10=sum(names(tbv3)[1:10]%in%names(pheno3)[1:N4])/10
rate25=sum(names(tbv3)[1:25]%in%names(pheno3)[1:N4])/25
rate50=sum(names(tbv3)[1:50]%in%names(pheno3)[1:N4])/50
rate100=sum(names(tbv3)[1:100]%in%names(pheno3)[1:N4])/100
rate200=sum(names(tbv3)[1:200]%in%names(pheno3)[1:N4])/200
rate=sum(names(tbv3)[1:N4]%in%names(pheno3)[1:N4])/N4


rateNP10=sum(names(tbv3)[1:10]%in%names(TBV))/10
rateNP25=sum(names(tbv3)[1:25]%in%names(TBV))/25
rateNP50=sum(names(tbv3)[1:50]%in%names(TBV))/50
rateNP100=sum(names(tbv3)[1:100]%in%names(TBV))/100
rateNP200=sum(names(tbv3)[1:200]%in%names(TBV))/200

progeny.qtl=progeny[,effects!=0]
nbqtl200=mean(apply(progeny.qtl,1,sum))
nbqtl100=mean(apply(progeny.qtl[1:100,],1,sum))
nbqtl50=mean(apply(progeny.qtl[1:50,],1,sum))
nbqtl10=mean(apply(progeny.qtl[1:10,],1,sum))
nbqtl25=mean(apply(progeny.qtl[1:25,],1,sum))

nb.al=length(unlist(apply(progeny,2,unique)))/(2*ncol(progeny))
var.tbv=var(TBV)

strategy=paste(header2,header,sep="_")

res=c(strategy,scenario,run,year,parameters["CG"],parameters["K"],bycross4,bycrossp,size4,sizep,h2,NC,parameters["N2"],N3,N4,TBV10,TBV25,TBV50,TBV100,TBV200,rate10,rate25,rate50,rate100,rate200,rate,rateNP10,rateNP25,rateNP50,rateNP100,rateNP200,nbqtl10,nbqtl25,nbqtl50,nbqtl100,nbqtl200,nb.al,accu,rmse,var.tbv)
write.table(t(data.frame(res)),paste(header2,"_",header,"_scenario",scenario,"_run",run,"_stat.txt",sep=""),append=T,col.names=F,row.names=F,quote=F,sep="\t")



###############
#################

rownames(progeny)=names
fam=cbind(names,fam)

result=vector("list",2)
result[[1]]=fam
result[[2]]=progeny
return(result)
}





#a balanced number for each cross
balance <- function(N,NC)
{
nbs <- rep(round(N/NC),NC)
dif <- N-sum(nbs)
 if (dif!=0) {
  for (i in 1:abs(dif)) {
  nbs[i] <- nbs[i]+(dif/abs(dif))
  }
 }
nbs
}


#to check if there is enough individuals
proportional <- function(N,NC,value.sp)
{
av=unlist(lapply(value.sp,mean))

ord=rev(order(av))

av=scale(av)
av <- av-(min(av))
nbs <- round(av*(N/sum(av)))
dif <- N-sum(nbs)
 if (dif!=0) {
  for (i in 1:abs(dif)) {
  nbs[i] <- nbs[i]+(dif/abs(dif))
  }
}


len=unlist(lapply(value.sp,length))

n=sum(nbs>len)
if (n>0)
{
torm=which(nbs>len)
nbs[torm]=len[torm]
n=N-sum(nbs)
sel=which(nbs==0)
toget=ord[ord%in%sel][1:n]
nbs[toget]=1
}


return(nbs)


}



pro=function(cr,npro=1000,chr,geno,map)
{
map.sp=split(map,chr)
res=vector("list",nrow(cr))

for (i in 1:nrow(cr))
{

p1.tmp.sp=split(geno[cr[i,1],],chr)
p2.tmp.sp=split(geno[cr[i,2],],chr)


res[[i]]=t(sapply(1:npro,function(j) gameteF1_CO_random.sp(map.sp,p1.tmp.sp,p2.tmp.sp)))
}
res
}

randomize_effect <- function(Nmarker,NQTL) {
Mu=rep(0,Nmarker)
Mu[sample(Nmarker,NQTL)] <- rnorm(NQTL)
return(Mu)
}

randomize_effect2 <- function(Nmarker,NQTL) {
Mu=rep(0,Nmarker)
Mu[sample(Nmarker,NQTL)] <- rnorm(NQTL,mean=0,sd=0.5)
return(Mu)
}

pros=function(cr,npros,chr,geno,map)
{
map.sp=split(map,chr)
res=vector("list",nrow(cr))

for (i in 1:nrow(cr))
{

p1.tmp.sp=split(geno[cr[i,1],],chr)
p2.tmp.sp=split(geno[cr[i,2],],chr)


res[[i]]=t(sapply(1:npros[i],function(j) gameteF1_CO_random.sp(map.sp,p1.tmp.sp,p2.tmp.sp)))
print(i)
}
res
}

randomize_effect <- function(Nmarker,NQTL) {
Mu=rep(0,Nmarker)
Mu[sample(Nmarker,NQTL)] <- rnorm(NQTL)
return(Mu)
}



gameteF1_CO_random.sp <- function(map.sp,p1.sp,p2.sp) {

res <- NULL
	for (i in 1:length(map.sp))
	{
		if (isTRUE(sample(c(FALSE,TRUE),1))) {
		p <- p1.sp[[i]]
		palt <- p2.sp[[i]]
		} else {
		p <- p2.sp[[i]]
		palt <- p1.sp[[i]]
		}
			if (isTRUE(sample(c(FALSE,TRUE),1))) {
			res <- c(res,p)} else {
			pos <- map.sp[[i]][,2]
			pos.max <- max(pos)
			nb.CO <- rpois(1,(2*pos.max/100))
				if (nb.CO==0) {
				res <- c(res,p)
				} else {
				
				pos.CO <- runif(nb.CO, 0, pos.max)
				pos.CO <- pos.CO[order(pos.CO)]
				test <- FALSE
				tmp <- rep(FALSE,length(pos))
					for (j in pos.CO) {
					test <- (!test)
					tmp[pos>j] <- test
					}
					p[tmp] <- palt[tmp]	
					res <- c(res,p)				
				}
		
			}
	}	
return(res)
}


generate_BV <- function(X,effect) {
BV <- as.matrix(X)%*%effect
return(BV)
}

generate_BV_gblup <- function(geno.train,pheno.train,geno.pred) {

nb=nrow(geno.pred)
M=rbind(geno.pred,geno.train)
rownames(M)=1:nrow(M)
y=c(rep(NA,nb),pheno.train)
A <- A.mat(M,min.MAF=0.01)
data <- data.frame(y=y,gid=rownames(M))
gblup=kin.blup(data=data,geno="gid",pheno="y",K=A)$g
BV <- gblup[1:nb]
return(BV)
}



#function to calculate the phenotypes


prod_noise=function(TBV,h2) {
sde=sqrt(((1-h2)/h2)*var(TBV))
pheno <- TBV + rnorm(length(TBV),mean=0,sd=sde)

return(pheno)
}



prod_noise2=function(TBV,h2,NREP) {
sde=sqrt(((1-h2)/h2)*var(TBV)/NREP)
pheno <- TBV + rnorm(length(TBV),mean=0,sd=sde)
return(pheno)
}


#marker effects

estimate_effect <- function(Y,X) {
effect <- mixed.solve(Y,as.matrix(X))$u
return(effect)
}

cross_random2 <- function(Nind,NC) {
cross.list <- cbind(sample(Nind,NC,replace=TRUE),sample(Nind,NC,replace=TRUE))
nam=paste(as.vector(cross.list[,1]),as.vector(cross.list[,2]),sep="_")
cross.list=cross.list[match(unique(nam),nam),]
	while(sum(cross.list[,1]==cross.list[,2])>0 | nrow(cross.list)<NC)
	{
	cross.list <- cbind(sample(Nind,NC,replace=TRUE),sample(Nind,NC,replace=TRUE))
	nam=paste(as.vector(cross.list[,1]),as.vector(cross.list[,2]),sep="_")
	cross.list=cross.list[match(unique(nam),nam),]
	}



return(cross.list)
}





cross_random3 <- function(parents,NC,NP,thresh=5)
{

count=0
parents=sample(parents,NP)


cr=sample(parents,2)
cr=cr[order(cr)]
pairs=paste(cr[1],cr[2],sep="/")
cr=t(as.matrix(cr))

	while (nrow(cr)<NC)
	{
	tmp=sample(parents,2)
	tmp=tmp[order(tmp)]
	pair.tmp=paste(tmp[1],tmp[2],sep="/")
	if (!pair.tmp%in%pairs)
	{
	pairs=c(pairs,pair.tmp)
	cr=rbind(cr,tmp)
	tab=table(as.vector(as.matrix(cr)))
	torm=names(tab)[tab>thresh]
	if (length(torm)>0)
	{
	parents=parents[!parents%in%torm]
	}
	}
	print(nrow(cr))
	}
cr
}



###############
cross_value_best_chr2 <- function(cross.opt,NC,NP,thresh=5) 
{
cross.opt=data.frame(cross.opt)
tmp=cross.opt[1,1:2]
cross.opt=cross.opt[-1,]

while(nrow(tmp)<NC)
{
toadd=as.vector(cross.opt[1,1:2])
if (length(unique(c(as.vector(as.matrix(tmp)),toadd)))<=NP)
{
tmp=rbind(tmp,toadd)
}
cross.opt=cross.opt[-1,]

test=table(as.vector(as.matrix(tmp)))
torm=names(test)[which(test>thresh)]

if (length(torm)>1)
{
cross.opt=cross.opt[(!cross.opt[,1]%in%torm) & (!cross.opt[,2]%in%torm),]

} 


}
return(tmp)
}


############






#random crosses


cross2 <- function(li1,li2,NC)
{
cross.list=cbind(sample(li1,NC,replace=TRUE),sample(li2,NC,replace=TRUE))
nam=paste(as.vector(cross.list[,1]),as.vector(cross.list[,2]),sep="_")
cross.list=cross.list[match(unique(nam),nam),]
	while(sum(cross.list[,1]==cross.list[,2])>0 | nrow(cross.list)<NC)
	{
	cross.list <- cbind(sample(li1,NC,replace=TRUE),sample(li2,NC,replace=TRUE))
	nam=paste(as.vector(cross.list[,1]),as.vector(cross.list[,2]),sep="_")
	cross.list=cross.list[match(unique(nam),nam),]
	}



return(cross.list)


}

#map A data.frame with 2 columns, standing for chromosome and position in cM respectively.

gameteF1_CO_random <- function(map,p1,p2) {
p1.sp <- split(p1,map[,1])
p2.sp <- split(p2,map[,1])
map.sp <- split(map,map[,1])
res <- NULL
	for (i in 1:length(map.sp))
	{
		if (isTRUE(sample(c(FALSE,TRUE),1))) {
		p <- p1.sp[[i]]
		palt <- p2.sp[[i]]
		} else {
		p <- p2.sp[[i]]
		palt <- p1.sp[[i]]
		}
			if (isTRUE(sample(c(FALSE,TRUE),1))) {
			res <- c(res,p)} else {
			pos <- map.sp[[i]][,2]
			pos.max <- max(pos)
			nb.CO <- rpois(1,(2*pos.max/100))
				if (nb.CO==0) {
				res <- c(res,p)
				} else {
				
				pos.CO <- runif(nb.CO, 0, pos.max)
				pos.CO <- pos.CO[order(pos.CO)]
				test <- FALSE
				tmp <- rep(FALSE,length(pos))
					for (j in pos.CO) {
					test <- (!test)
					tmp[pos>j] <- test
					}
					p[tmp] <- palt[tmp]	
					res <- c(res,p)				
				}
		
			}
	}	
return(res)
}



gameteF1_CO_random.sp <- function(map.sp,p1.sp,p2.sp) {

res <- NULL
	for (i in 1:length(map.sp))
	{
		if (isTRUE(sample(c(FALSE,TRUE),1))) {
		p <- p1.sp[[i]]
		palt <- p2.sp[[i]]
		} else {
		p <- p2.sp[[i]]
		palt <- p1.sp[[i]]
		}
			if (isTRUE(sample(c(FALSE,TRUE),1))) {
			res <- c(res,p)} else {
			pos <- map.sp[[i]][,2]
			pos.max <- max(pos)
			nb.CO <- rpois(1,(2*pos.max/100))
				if (nb.CO==0) {
				res <- c(res,p)
				} else {
				
				pos.CO <- runif(nb.CO, 0, pos.max)
				pos.CO <- pos.CO[order(pos.CO)]
				test <- FALSE
				tmp <- rep(FALSE,length(pos))
					for (j in pos.CO) {
					test <- (!test)
					tmp[pos>j] <- test
					}
					p[tmp] <- palt[tmp]	
					res <- c(res,p)				
				}
		
			}
	}	
return(res)
}


cross_value_best_chr <- function(estimated.effect,X,map) {

X=as.matrix(X)
gebv.best <- NULL
cross.list <- NULL
Nind=nrow(X)
date()
	for (i in 1:(Nind-1))
	{ 
print(i)
		for (j in (i+1):Nind)
		{
		
		gebv.best <- c(gebv.best,gameteF1_noCO_best(map,as.vector(X[i,]),as.vector(X[j,]),estimated.effect)%*%estimated.effect)
		
		cross.list <- rbind(cross.list,c(i,j))
		}
	}
date()
	cross.list=cbind(cross.list,gebv.best)

cross.list=cross.list[rev(order(gebv.best)),]
return(cross.list)
}





cross2_value_best_chr <- function(estimated.effect,X1,X2,map) {

X1=as.matrix(X1)
X2=as.matrix(X2)

gebv.best <- NULL
cross.list <- NULL
Nind1=nrow(X1)
Nind2=nrow(X2)
date()
	for (i in 1:Nind1)
	{ 
print(i)
		for (j in 1:Nind2)
		{
		
		gebv.best <- c(gebv.best,gameteF1_noCO_best(map,as.vector(X1[i,]),as.vector(X2[j,]),estimated.effect)%*%estimated.effect)
		
		cross.list <- rbind(cross.list,c(i,j))
		}
	}
date()
	cross.list=cbind(cross.list,gebv.best)
cross.list=cross.list[rev(order(gebv.best)),]
return(cross.list)
}



cross2_value_min_chr <- function(estimated.effect,X1,X2,map) {

X1=as.matrix(X1)
X2=as.matrix(X2)

gebv.best <- NULL
cross.list <- NULL
Nind1=nrow(X1)
Nind2=nrow(X2)
date()
	for (i in 1:Nind1)
	{ 
print(i)
		for (j in 1:Nind2)
		{
		
		gebv.best <- c(gebv.best,gameteF1_noCO_min(map,as.vector(X1[i,]),as.vector(X2[j,]),estimated.effect)%*%estimated.effect)
		
		cross.list <- rbind(cross.list,c(i,j))
		}
	}
date()
	cross.list=cbind(cross.list,gebv.best)
cross.list=cross.list[order(gebv.best),]
return(cross.list)
}




cross_value_best_chr2 <- function(cross.opt,NC,NP,thresh=5) 
{
cross.opt=data.frame(cross.opt)
tmp=cross.opt[1,1:2]
cross.opt=cross.opt[-1,]

while(nrow(tmp)<NC)
{
toadd=as.vector(cross.opt[1,1:2])
if (length(unique(c(as.vector(as.matrix(tmp)),toadd)))<=NP)
{
tmp=rbind(tmp,toadd)
}
cross.opt=cross.opt[-1,]

test=table(as.vector(as.matrix(tmp)))
torm=names(test)[which(test>thresh)]

if (length(torm)>1)
{
cross.opt=cross.opt[(!cross.opt[,1]%in%torm) & (!cross.opt[,2]%in%torm),]

} 


}
return(tmp)
}



gameteF1_noCO_best <- function(map,p1,p2,effect) {
p1.sp <- split(p1,map[,1])
p2.sp <- split(p2,map[,1])
map.sp <- split(map,map[,1])
effect.sp <- split(effect,map[,1])
res <- NULL

	for (i in 1:length(map.sp))
	{
	p1.tmp <- p1.sp[[i]]
	p2.tmp <- p2.sp[[i]]
	effect.tmp <- effect.sp[[i]]
	gebv1 <- c(p1.tmp%*%effect.tmp)
	gebv2 <- c(p2.tmp%*%effect.tmp)
	if (gebv1>=gebv2)
	{
	res <- c(res,p1.tmp)
	
	} else {
	res <- c(res,p2.tmp)
	}			
	}	
return(res)
}



gameteF1_noCO_min <- function(map,p1,p2,effect) {
p1.sp <- split(p1,map[,1])
p2.sp <- split(p2,map[,1])
map.sp <- split(map,map[,1])
effect.sp <- split(effect,map[,1])
res <- NULL

	for (i in 1:length(map.sp))
	{
	p1.tmp <- p1.sp[[i]]
	p2.tmp <- p2.sp[[i]]
	effect.tmp <- effect.sp[[i]]
	gebv1 <- c(p1.tmp%*%effect.tmp)
	gebv2 <- c(p2.tmp%*%effect.tmp)
	if (gebv1>=gebv2)
	{
	res <- c(res,p2.tmp)
	
	} else {
	res <- c(res,p1.tmp)
	}			
	}	
return(res)
}


cross.SG=function(X,estimated_effect,map,NC)
{

X=as.matrix(X)
estimated.effect=as.vector(as.matrix(estimated_effect))
gebv.best=NULL
cross.list=NULL
Nind=nrow(X)
for (i in 1:(Nind-1))
{ 
for (j in (i+1):Nind)
{
gebv.best=c(gebv.best,gamete.best.chr(map,as.vector(X[i,]),as.vector(X[j,]),estimated.effect)%*%estimated.effect)
cross.list=rbind(cross.list,c(i,j))
}
}
cross.list=cross.list[rev(order(gebv.best))[1:NC],]
cross.list
}





res.cycle=function(pattern,effects)
{
files=Sys.glob(pattern)
years=NULL
mean.tbv=NULL
top.tbv=NULL

for (i in files)
{
df=read.table(i)
test=strsplit(i,"_")[[1]]
loc=grep("year",test)
years=c(years,gsub("year","",strsplit(i,"_")[[1]][loc]))
tbv=generate_BV(as.matrix(df),effects)
mean.tbv=c(mean.tbv,mean(tbv))
top.tbv=c(top.tbv,mean(tbv[rev(order(tbv))][1:10]))
}


result=cbind.data.frame(years,mean.tbv,top.tbv)
result[,1]=as.numeric(as.vector(result[,1]))
result=result[order(result[,1]),]
result

}






cycle_GS_withqtl_comb=function(header2,year=2,cross,pro.sp,parameters,scenario,run,estimated_effects,names=1001:1100,n3s,map3)
{

parameter=parameters[scenario,]

N4=parameters[scenario,"GS.N4"]
NC=parameters[scenario,"GS.NC"]
NP=parameters[scenario,"NP"]
h2=parameters[scenario,"h2"]
bycross4=parameters[scenario,"bycross4"]
bycrossp=parameters[scenario,"bycrossp"]
size4=parameters[scenario,"size4"]
sizep=parameters[scenario,"sizep"]
crossing=parameters[scenario,"crossing"]



fam=cross[rep(1:nrow(cross),n3s),1:2]




gebv.tmp=vector("list",NC)

for (i in 1:NC)
{
gebv.tmp[[i]]=generate_BV(pro.sp[[i]],estimated_effects)

}



best=NULL
for (i in 1:NC)
{
best=rbind(best,pro.sp[[i]][which.max(gebv.tmp[[i]]),])

}


cr2=cross_value_best_chr(estimated_effects,as.matrix(best),map3)
cr3=cr2[1:NC,1:2]





sel=unique(as.vector(as.matrix(cr3)))
progeny=best[sel,]
#fam=fam[sel,]

corres=cbind(sel,1:length(sel))
cr4=cbind(corres[match(cr3[,1],corres[,1]),2],corres[match(cr3[,2],corres[,1]),2])

rownames(progeny)=names[1]:(names[1]+length(sel)-1)
fam=cbind(rownames(progeny),cross[sel,])


nam=paste(header2,"_GS_h2_",h2,"_scenario",scenario,"_bycross4=",bycross4,"_bycrossp=",bycrossp,"_size4=",size4,"_sizep=",sizep,"_run",run,"_year",year,"_crossing=",crossing,sep="")

write.table(progeny,paste(nam,"_progeny.txt",sep=""),quote=F,sep="\t")
write.table(fam,paste(nam,"_pedigree.txt",sep=""),quote=F,sep="\t",row.names=F,col.names=F)
write.table(cr4,paste(nam,"_cross.txt",sep=""),quote=F,sep="\t",row.names=F,col.names=F)


}


nb.al=function(mat)
{
sum(apply(mat,2,function(i) length(unique(i))))
}



av.sim=function(mat)
{

mean(mat[,4]/mat[,3])

}

av.he=function(mat)
{


}

mse=function(TBV,GEBV)
{
(sum((TBV-GEBV)**2)/length(TBV))**0.5
}

ac=function(TBV,GEBV)
{
cor(TBV,GEBV)
}


rate.best=function(TBV,GEBV,nb.sel)
{
length(intersect(rev(order(TBV))[1:nb.sel], rev(order(GEBV))[1:nb.sel]))/nb.sel
}
