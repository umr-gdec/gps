#include <Rcpp.h>

#include <algorithm>

#define STATS_GO_INLINE
#include "stats.hpp"

class WriteBuffer
{
public:

	virtual ~WriteBuffer() {}

	virtual void write(char data) = 0;
	virtual void endl() = 0;
};

class ReadBuffer
{
public:

	virtual ~ReadBuffer() {}

	virtual bool isValid() const = 0;
	virtual Rcpp::NumericMatrix readAll() = 0;
};

/**
 * Write to file content with intermediate buffer with one line per progeny.
 */
class WriteTextBuffer : public WriteBuffer
{
public:

	WriteTextBuffer(const std::string &filename, int width, int height) :
		m_file(nullptr),
		m_pos(0),
		m_buffer(new char[4096]),
		m_width(width),
		m_height(height)
	{
		m_file = fopen(filename.c_str(), "wb");
	}

	virtual ~WriteTextBuffer()
	{
		if (m_pos > 0) {
			flush();
		}

		delete [] m_buffer;
		fclose(m_file);
	}

	virtual void write(char data) override
	{
		m_buffer[m_pos++] = data;

		if (m_pos >= 4096) {
			flush();
		}
	}

	virtual void endl() override
	{
		m_buffer[m_pos++] = '\n';

		if (m_pos >= 4096) {
			flush();
		}
	}

private:

	void flush()
	{
		// if (m_pos <= 0) {
		// 	return;
		// }

		fwrite(m_buffer, 1, m_pos, m_file);
		m_pos = 0;
	}

	FILE *m_file;
	int m_pos;
	char *m_buffer;

	int m_width;
	int m_height;
};

/**
 * Read from file of text format.
 */
class ReadTextBuffer : public ReadBuffer
{
public:

	ReadTextBuffer(const std::string &filename) :
		m_file(nullptr),
		m_buffer(new char[4096]),
		m_pos(0),
		m_row(0),
		m_col(0),
		m_width(0),
		m_height(0)
	{
		m_file = fopen(filename.c_str(), "rb");

		// get num row and cols
		detect();
	}

	virtual ~ReadTextBuffer()
	{
		delete [] m_buffer;
		fclose(m_file);
	}

	virtual bool isValid() const override
	{
		return m_width && m_height;
	}

	virtual Rcpp::NumericMatrix readAll() override
	{
		Rcpp::NumericMatrix res(m_height, m_width);

		m_row = 0;
		m_col = 0;

		while (readChunk(res)) {}

		return res;
	}

private:

	bool readChunk(Rcpp::NumericMatrix &out)
	{
		int size = fread(m_buffer, 1, 4096, m_file);
		if (size <= 0) {
			return false;
		}

		char data;

		for (int i = 0; i < size; ++i) {
			data = m_buffer[i];
			if (data == '\n') {
				++m_row;
				m_col = 0;
			} else {
				out(m_row, m_col) = (float)(data - '0');
				// out[m_pos++] = (float)(data - '0');
				++m_col;
			}
		}

		return true;
	}

	void detect()
	{
		int size;
		char data;

		while (1) {
			size = fread(m_buffer, 1, 4096, m_file);
			if (size <= 0) {
				break;
			}

			for (int i = 0; i < size; ++i) {
				data = m_buffer[i];
				if (data == '\n') {
					++m_row;

					m_width = std::max(m_col, m_width);
					m_height = std::max(m_row, m_height);

					m_col = 0;
				} else {
					++m_col;
				}
			}
		}

		fseek(m_file, 0, SEEK_SET);
	}

	FILE *m_file;
	char *m_buffer;

	int m_pos;
	int m_row;
	int m_col;

	int m_width;
	int m_height;
};

/**
 * Write to file content with intermediate buffer.
 * @note In little endian.
 * @todo file header with magic + num row and columns
 * @todo Compressed 2 bits per component version
 * 0 => 1, 1 => 2, 2 => 3, padding => 0
 */
class WriteCompressedBuffer : public WriteBuffer
{
public:

	static constexpr const char* MAGIC = "SOR0";

	WriteCompressedBuffer(const std::string &filename, int width, int height) :
		m_file(nullptr),
		m_pos(0),
		m_buffer(new char[4096]),
		m_curBitPos(0),
		m_width(width),
		m_height(height)
	{
		m_file = fopen(filename.c_str(), "wb");

		// header
		fwrite(MAGIC, 1, 4, m_file);
		fwrite(&width, 4, 1, m_file);
		fwrite(&height, 4, 1, m_file);
	}

	virtual ~WriteCompressedBuffer()
	{
		if (m_pos > 0 || m_curBitPos >= 2) {
			// flush with the last incomplete byte
			flushWithRemaining();
		}

		delete [] m_buffer;
		fclose(m_file);
	}

	virtual void write(char data) override
	{
		m_buffer[m_pos] |= ((data+1) & 3) << m_curBitPos;
		m_curBitPos += 2;

		if (m_curBitPos >= 8) {
			if (m_pos >= 4095) {
				// flush when buffer is full and current byte too
				flush();
			} else {
				++m_pos;                    // next byte
				m_curBitPos = 0;            // first bits
				m_buffer[m_pos] = 0;        // init to full of padding
			}
		}
	}

	virtual void endl() override
	{
		// not in compressed mode
	}

private:

	void flush()
	{
		// // and flush the remaining byte
		// if ((m_pos == 0) && (m_curBitPos == 0)) {
		// 	// dont flush when buffer is empty and current byte too
		// 	return;
		// }

		unsigned size = m_pos + ((m_curBitPos >= 8) ? 1 : 0);
		fwrite(m_buffer, 1, size, m_file);

		m_pos = 0;
		m_curBitPos = 0;

		m_buffer[0] = 0;   // init to full of padding
	}

	void flushWithRemaining()
	{
		// and flush the remaining byte
		if ((m_pos == 0) && (m_curBitPos == 0)) {
			// dont flush when buffer is empty and current byte too
			return;
		}

		unsigned size = m_pos + ((m_curBitPos >= 2) ? 1 : 0);
		fwrite(m_buffer, 1, size, m_file);

		m_pos = 0;
		m_curBitPos = 0;

		m_buffer[0] = 0;   // init to full of padding
	}

	FILE *m_file;
	unsigned int m_pos;
	char *m_buffer;
	unsigned char m_curBitPos;

	int m_width;
	int m_height;
};

/**
 * Read from file of compressed binary format.
 */
class ReadCompressedBuffer : public ReadBuffer
{
public:

	ReadCompressedBuffer(const std::string &filename) :
		m_file(nullptr),
		m_pos(0),
		m_buffer(new char[4096]),
		m_width(0),
		m_height(0),
		m_err(false)
	{
		m_file = fopen(filename.c_str(), "rb");
		detect();
	}

	virtual ~ReadCompressedBuffer()
	{
		delete [] m_buffer;
		fclose(m_file);
	}

	virtual bool isValid() const override
	{
		return !m_err && m_width && m_height;
	}

	virtual Rcpp::NumericMatrix readAll() override
	{
		Rcpp::NumericMatrix res(m_height, m_width);

		m_row = m_col = 0;

		while (readChunk(res)) {}

		return res;
	}

private:

	void detect()
	{
		char magic[4];

		if (fread(magic, 1, 4, m_file) < 4) {
			m_err = true;
			return;
		}

		if (memcmp(magic, WriteCompressedBuffer::MAGIC, 4) != 0) {
			m_err = true;
			return;
		}

		if (fread(&m_width, 4, 1, m_file) < 1) {
			m_err = true;
			return;
		}

		if (fread(&m_height, 4, 1, m_file) < 1) {
			m_err = true;
			return;
		}

		printf("%i %i\n", m_width, m_height);
	}

	bool readData(char datas, Rcpp::NumericMatrix &out)
	{
		char data;

		for (char b = 0; b < 8; b += 2) {
			data = (datas >> b) & 3;

			if (!data) {
				// padding end reached
				return false;
			}

			out(m_row, m_col++) = data-1;  // data minus 1 (@see the writer)

			if (m_col >= m_width) {
				++m_row;
				m_col = 0;
			}

			if (m_row >= m_height) {
				return false;
			}
		}

		return true;
	}

	bool readChunk(Rcpp::NumericMatrix &out)
	{
		int size = fread(m_buffer, 1, 4096, m_file);
		if (size <= 0) {
			return false;
		}

		for (int i = 0; i < size; ++i) {
			if (!readData(m_buffer[i], out)) {
				break;
			}
		}

		return true;
	}

	FILE *m_file;
	int m_pos;
	char *m_buffer;

	int m_row;
	int m_col;

	int m_width;
	int m_height;

	bool m_err;
};

void processProgenyForChromosome(
	std::vector<char> &out,
	float maxPos,
	const std::vector<float> &pos,
	std::vector<char> &p1,
	std::vector<char> &p2);

// [[Rcpp::export]]
int gameteF1_CO_random(   // Rcpp::NumericMatrix Rcpp::List
		const Rcpp::NumericMatrix &map,
		const Rcpp::NumericVector &p1,
		const Rcpp::NumericVector &p2,
		int num_progeny,
		const std::string &filename,
		bool compressed = true)
{
    const Rcpp::NumericVector map_0 = map(Rcpp::_, 0);   // col 0 of map
    const Rcpp::NumericVector map_1 = map(Rcpp::_, 1);   // col 1 of map

	int num_chrom = 0;
	int num_pos = (int)map_0.length();

	for (int i = 0; i < map_0.length(); ++i) {
		num_chrom = std::max(num_chrom, (int)map_0[i]);
	}

	// col 1 of map by chromosome
	std::vector<std::vector<float>> map_by_chrom;
	map_by_chrom.resize(num_chrom);

	// max pos in map (of col 1) by chromosome
	std::vector<float> max_by_chrom(num_chrom);

	// parent1 by chromosome
	std::vector<std::vector<char>> p1_by_chrom;
	p1_by_chrom.resize(num_chrom);

	// parent2 by chromosome
	std::vector<std::vector<char>> p2_by_chrom;
	p2_by_chrom.resize(num_chrom);

	unsigned int chr;
	float max;

	// split matrix per chromosome
	for (unsigned int i = 0; i < (unsigned int)map_0.length(); ++i) {
		chr = (unsigned int)map_0[i] - 1;  // to 0 indexed
		
		map_by_chrom[chr].push_back(map_1[i]);

		p1_by_chrom[chr].push_back((char)p1[i]);
		p2_by_chrom[chr].push_back((char)p2[i]);

		// originalPosition[chr].push_back(i);

		// compute max per chromosome
		max = 0;

		for (size_t j = 0; j < map_by_chrom[chr].size(); ++j) {
			max = std::max<float>(max, map_by_chrom[chr][j]);
		}

		max_by_chrom[chr] = max;
	}

    // results by chromosome
    std::vector<std::vector<std::vector<char>>> res;

    for (unsigned int i = 0; i < (unsigned int)map_by_chrom.size(); ++i) {
    	const std::vector<float> &lmap = map_by_chrom[i];    // pos (second column of map for current chromosome)
    	float lmax = max_by_chrom[i];                        // max pos for current chromosome
		std::vector<char> &lp1 = p1_by_chrom[i];             // parent 1 for current chromosome
		std::vector<char> &lp2 = p2_by_chrom[i];             // parent 2 ...

		std::vector<std::vector<char>> resByChrom(num_progeny);

	    for (int n = 0; n < num_progeny; ++n) {
	    	resByChrom[n].resize(lmap.size());
	    	processProgenyForChromosome(resByChrom[n], lmax, lmap, lp1, lp2);
    	}

		res.push_back(resByChrom);
   	}
   	
#if 0
   	FILE *f;
   	char fileName[512];
   	char data;
   	const char endl = '\n';

	// write res matrices one file per chromosome
   	for (unsigned int i = 0; i < res.length(); ++i) {
   		Rcpp::NumericMatrix mat = res[i];

		sprintf(fileName, "%s_chrom_%i", filename.c_str(), i+1);
   		f = fopen(fileName, "wt");

   		int ofs = 0;

   		for (int j = 0; j < mat.rows(); ++j) {
   			for (int k = 0; k < mat.cols(); ++k) {
   				data = mat[ofs++] + '0';
   				fwrite(&data, 1, 1, f);
   			}

   			fwrite(&endl, 1, 1, f);
   		}

   		fclose(f);
   	}
#endif

   	// write a text format non optimized or in compressed/optimized buffer
   	WriteBuffer *outputFile = nullptr;

   	if (compressed) {
		outputFile = new WriteCompressedBuffer(filename, num_pos, num_progeny);
   	} else {
		outputFile = new WriteTextBuffer(filename, num_pos, num_progeny);   		
   	}

   	// merge chromosomes
   	// Rcpp::NumericMatrix mergedMat(num_progeny, num_pos);

   	int pos;
   	char data;

   	std::vector<int> tmpPos(num_chrom, 0);

	for (int n = 0; n < num_progeny; ++n) {
		// reset counter
		std::fill(tmpPos.begin(), tmpPos.end(), 0);

		// @todo could be optimized using a buffer before writing
	   	for (int i = 0; i < num_pos; ++i) {
   			chr = map_0[i] - 1;
   			pos = tmpPos[chr];

   			// ref on matrix for current chromosome
   			data = res[chr][n][pos] + '0';
			outputFile->write(data);

   			// does we need the merged mat in returns ?
			// mergedMat(n, i) = (int)mat(n, pos);

   			// in pos for current chromosome
			++tmpPos[chr];
   		}

   		// new line per progeny
   		outputFile->endl();
   	}

   	delete outputFile;

   	return 0;
}

void processProgenyForChromosome(
	std::vector<char> &out,
	float maxPos,
	const std::vector<float> &pos,
	std::vector<char> &p1,
	std::vector<char> &p2)
{
	std::vector<char> *p = nullptr;
	std::vector<char> *palt = nullptr;

	// pour chaque chromosome, une chance sur 2 de récupérer le chromosome du parent 1 ou 2
	if ((rand() % 2) == 1) {
		p = &p1;
		palt = &p2;
	} else {
		p = &p2;
		palt = &p1;
	}

	// pour chaque bras de chromosome, une chance sur 2 qu'il subisse un CO
	if ((rand() % 2) == 1) {
		for (size_t i = 0; i < p->size(); ++i) {
			out[i] = (*p)[i];
		}
	} else {
		unsigned int numCo = (unsigned int)stats::rpois<float>(2*maxPos/100);
		if (numCo == 0) {
			for (size_t i = 0; i < p->size(); ++i) {
				out[i] = (*p)[i];
			}
		} else {
			std::vector<float> posCo(numCo);
			stats::runif_int<float>(0, maxPos, posCo.data(), numCo);

			std::sort(posCo.begin(), posCo.end());

			bool toggle = false;
			for (size_t j = 0; j < posCo.size(); ++j) {
				toggle = !toggle;

				for (size_t k = 0; k < pos.size(); ++k) {
					if (pos[k] > posCo[j]) {
						// tout ceux > j une fois sur deux
						if (toggle) {
							out[k] = (*palt)[k];
							// printf("disp1 ");
						} else {
							out[k] = (*p)[k];
							// printf("disp2 ");
						}
					} else {
						out[k] = (*p)[k];
						// printf("disp3 ");
					}
				}
			}
		}
	}
}

// [[Rcpp::export]]
Rcpp::NumericMatrix sor_read_file(const std::string &fileName, bool compressed=true)
{
	Rcpp::NumericMatrix res;
	ReadBuffer *reader = nullptr;

	if (compressed) {
		reader = new ReadCompressedBuffer(fileName);
	} else {
		reader = new ReadTextBuffer(fileName);
	}

	if (reader->isValid()) {
		res = reader->readAll();
	}
	
	if (reader) {
		delete reader;
	}

	return res;
}
