// Generated by using Rcpp::compileAttributes() -> do not edit by hand
// Generator token: 10BE3573-1514-4C36-9D1C-5A225CD40393

#include <Rcpp.h>

using namespace Rcpp;

// gameteF1_CO_random
int gameteF1_CO_random(const Rcpp::NumericMatrix& map, const Rcpp::NumericVector& p1, const Rcpp::NumericVector& p2, int num_progeny, const std::string& filename, bool compressed);
RcppExport SEXP _sor_gameteF1_CO_random(SEXP mapSEXP, SEXP p1SEXP, SEXP p2SEXP, SEXP num_progenySEXP, SEXP filenameSEXP, SEXP compressedSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const Rcpp::NumericMatrix& >::type map(mapSEXP);
    Rcpp::traits::input_parameter< const Rcpp::NumericVector& >::type p1(p1SEXP);
    Rcpp::traits::input_parameter< const Rcpp::NumericVector& >::type p2(p2SEXP);
    Rcpp::traits::input_parameter< int >::type num_progeny(num_progenySEXP);
    Rcpp::traits::input_parameter< const std::string& >::type filename(filenameSEXP);
    Rcpp::traits::input_parameter< bool >::type compressed(compressedSEXP);
    rcpp_result_gen = Rcpp::wrap(gameteF1_CO_random(map, p1, p2, num_progeny, filename, compressed));
    return rcpp_result_gen;
END_RCPP
}
// sor_read_file
Rcpp::NumericMatrix sor_read_file(const std::string& fileName, bool compressed);
RcppExport SEXP _sor_sor_read_file(SEXP fileNameSEXP, SEXP compressedSEXP) {
BEGIN_RCPP
    Rcpp::RObject rcpp_result_gen;
    Rcpp::RNGScope rcpp_rngScope_gen;
    Rcpp::traits::input_parameter< const std::string& >::type fileName(fileNameSEXP);
    Rcpp::traits::input_parameter< bool >::type compressed(compressedSEXP);
    rcpp_result_gen = Rcpp::wrap(sor_read_file(fileName, compressed));
    return rcpp_result_gen;
END_RCPP
}

static const R_CallMethodDef CallEntries[] = {
    {"_sor_gameteF1_CO_random", (DL_FUNC) &_sor_gameteF1_CO_random, 6},
    {"_sor_sor_read_file", (DL_FUNC) &_sor_sor_read_file, 2},
    {NULL, NULL, 0}
};

RcppExport void R_init_sor(DllInfo *dll) {
    R_registerRoutines(dll, NULL, CallEntries, NULL, NULL);
    R_useDynamicSymbols(dll, FALSE);
}
