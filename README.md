# About


These scripts were used to run simulations described in: 
"Integration of genomic selection into winter-type bread wheat breeding schemes: a simulation pipeline including economic constraints"
Sarah Ben-Sadoun, Aline Fugeray-Scarbel, Jérôme Auzanneau, Gilles Charmet, Stéphane Lemarié, Sophie Bouchet 

* PS_rep1.r is a script used to simulate a bread wheat breeding scheme using phenotypic selection.
* GPS_random_rep1.r and GPS_opt_rep1.r are scripts used to simulate a bread wheat breeding scheme using genomic selection and phenotypic selection.
* GPS_opt_rep1.r is used to simulate a breeding program in which crosses are optimized using genomic predictions.

3 cycles of 5 years selection were simulated for each breeding program. 

* function_opt.r and sor directory include functions that are used in PS_rep1.r, GPS_random_rep1.r and GPS_opt_rep1.r scripts.
* FilesToRunScripts directory includes examples of files that are needed to run PS_rep1.r, GPS_random_rep1.r and GPS_opt_rep1.r scripts.


# How to run 

You can define the number of the run and the number of scenario.
If these arguments are not defined, the first run and the first scenario are used for the simulation.

The path of the function_opt.r script and of the sor directory must be changed.
 
The path of the files must be changed.


# Feedback

If you find bugs or have comments, please contact <sophie.bouchet@inrae.fr>
